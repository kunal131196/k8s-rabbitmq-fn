# Introduction


# Requirements

1.  Helm
2.  HPA version v2beta2

# Deployment

# Steps to deploy everything

# 1) Create docker image for application

```
cd App
docker build -t pythonapp .
```

# 2) Steps to Deploy app on k8s 

```

helm repo add bitnami https://charts.bitnami.com/bitnami

kubectl create ns rabbitmq-fortinet

helm upgrade --install --namespace rabbitmq-fortinet rabbitmq-server-fortinet bitnami/rabbitmq  -f charts/rabbitmq/values.yaml --version 12.7.0

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm upgrade --install --namespace rabbitmq-fortinet prometheus-fortinet prometheus-community/prometheus -f charts/prometheus/values.yaml


helm upgrade --install --namespace rabbitmq-fortinet prometheus-adapter-fortinet prometheus-community/prometheus-adapter -f charts/prometheus-adapter/config.yaml

helm upgrade --install --namespace rabbitmq-fortinet rabbitmq-fortinet-app ./charts/rabbitmq-fortinet-app


```

